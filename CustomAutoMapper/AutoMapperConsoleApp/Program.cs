﻿using DomainLayer;
using BusinessLayer;
using System;

namespace AutoMapperConsoleApp
{

    public class Program
    {
        public static void Main()
        {

            RepoInteraction businessLayerObject = new RepoInteraction();

            OrderDTO odto;

            odto = businessLayerObject.GetOrderDTO();

            Console.WriteLine(odto);
        }
    }
}

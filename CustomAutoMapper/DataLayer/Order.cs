﻿
namespace DataLayer
{
    
    public class Order
    {
        private int businessUID = 12;
        public string status;
        public int Id { private get; set; }
        public string CustomerName { get; set; }
        public override string ToString()
        {
            return Id + " " + CustomerName + " " + status;
        }
    }
}

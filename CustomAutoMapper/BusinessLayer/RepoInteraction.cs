﻿using DomainLayer;
using ConsoleAppRepoLayer;

namespace BusinessLayer
{
    public class RepoInteraction
    {
        public OrderDTO GetOrderDTO()
        {
            Mapper mapper = new Mapper();
            OrderDTO orderDTO = new OrderDTO();
            return mapper.Map<OrderDTO>(orderDTO);
        }
    }
}

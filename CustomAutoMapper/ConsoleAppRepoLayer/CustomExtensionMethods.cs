﻿using System;
using System.Reflection;

namespace ConsoleAppRepoLayer
{
    public static class CustomExtensionMethods
    {
        public static object TryGetValue(this FieldInfo fieldInfo, object source)
        {
            try
            {
                return fieldInfo.GetValue(source);
            }
            catch (FieldAccessException e)
            {
                return null;
            }
        }
        public static void TrySetValue(this FieldInfo fieldInfo, object destination, object value)
        {
            try
            {
                fieldInfo.SetValue(destination, value);
            }
            catch (FieldAccessException e)
            {
                return;
            }
        }
        public static object TryGetValue(this PropertyInfo propertyInfo, object source)
        {
            try
            {
                return propertyInfo.GetValue(source);
            }
            catch (ArgumentException e)
            {
                try
                {
                    FieldInfo field = source.GetType().GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);
                    return field.GetValue(source);
                }
                catch (ArgumentNullException ane)
                {
                    return null;
                }
            }
        }
        public static void TrySetValue(this PropertyInfo propertyInfo, object destination, object value)
        {
            try
            {
                propertyInfo.SetValue(destination, value);
            }
            catch (ArgumentException e)
            {
                try
                {
                    FieldInfo field = destination.GetType().GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);
                    field.SetValue(destination, value);
                }
                catch (ArgumentNullException ane)
                {
                    return;
                }
            }
        }

    }
}

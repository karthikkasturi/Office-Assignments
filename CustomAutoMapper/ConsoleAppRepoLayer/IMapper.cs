﻿
namespace ConsoleAppRepoLayer
{
    public interface IMapper
    {
        //T Map<T>(object source) where T : new();
        T Map<T>(object source, T destination);

        T Map<T>(T destination);

    }
}

﻿using System;
using System.Reflection;
using DataLayer;
namespace ConsoleAppRepoLayer
{
    public class Mapper : IMapper
    {
        //public T Map<T>(T destination) {
        //    return Map()
        //}


        public T Map<T>(T destination)
        {
            return Map(source: new Order() { CustomerName = "Karthik", Id = 102, status = "Completed" }
                      , destination: destination);
        }

        public T Map<T>(object source, T destination)
        {
            Type sourceType = source.GetType();
            Type destinationType = destination.GetType();

            FieldInfo[] sourceFields = sourceType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            PropertyInfo[] sourceProperties = sourceType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            FieldInfo[] destinationFields = destination.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            PropertyInfo[] destinationProperties = destinationType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);


            foreach (var sourceField in sourceFields)
            {

                foreach (var destinationField in destinationFields)
                {
                    if (sourceField.ToString() == destinationField.ToString())
                    {
                        object sourceFieldValue = sourceField.TryGetValue(source);
                        if (sourceField.FieldType.IsValueType || sourceField.FieldType.IsEnum || sourceField.FieldType.Equals(typeof(string)))
                        {
                            destinationField.TrySetValue(destination, sourceFieldValue);
                        }
                        else
                        {
                            destinationField.TrySetValue(destination, Map(sourceFieldValue, Activator.CreateInstance(sourceField.FieldType)));
                        }
                    }
                }
                foreach (var destinationProperty in destinationProperties)
                {
                    if (destinationProperty.ToString() == sourceField.ToString())
                    {
                        object sourceFieldValue = sourceField.TryGetValue(source);
                        if (sourceField.FieldType.IsValueType || sourceField.FieldType.IsEnum || sourceField.FieldType.Equals(typeof(string)))
                        {
                            destinationProperty.TrySetValue(destination, sourceFieldValue);
                        }
                        else
                        {
                            destinationProperty.TrySetValue(destination, Map(sourceFieldValue, Activator.CreateInstance(sourceField.FieldType)));
                        }
                    }
                }
            }

            foreach (var sourceProperty in sourceProperties)
            {
                foreach (var destinationField in destinationFields)
                {
                    if (sourceProperty.ToString() == destinationField.ToString())
                    {
                        object sourcePropertyValue = sourceProperty.TryGetValue(source);
                        if (sourceProperty.PropertyType.IsValueType || sourceProperty.PropertyType.IsEnum || sourceProperty.PropertyType.Equals(typeof(string)))
                        {
                            destinationField.TrySetValue(destination, sourcePropertyValue);
                        }
                        else
                        {
                            destinationField.TrySetValue(destination, Map(sourcePropertyValue, Activator.CreateInstance(sourceProperty.PropertyType)));
                        }
                        //destinationField.TrySetValue(destination, sourcePropertyValue);
                    }
                }
                foreach (var destinationProperty in destinationProperties)
                {
                    if (destinationProperty.ToString() == sourceProperty.ToString())
                    {
                        object sourcePropertyValue = sourceProperty.TryGetValue(source);
                        if (sourceProperty.PropertyType.IsValueType || sourceProperty.PropertyType.IsEnum || sourceProperty.PropertyType.Equals(typeof(string)))
                        {
                            destinationProperty.TrySetValue(destination, sourcePropertyValue);
                        }
                        else
                        {
                            destinationProperty.TrySetValue(destination, Map(sourcePropertyValue, Activator.CreateInstance(sourceProperty.PropertyType)));
                        }
                        //destinationProperty.TrySetValue(destination, sourcePropertyValue);
                    }
                }
            }
            return destination;
        }

    }

}

﻿using System;
using System.Reflection;
namespace DomainLayer
{
    public class OrderDTO
    {
        private int businessUID = 1;
        public string status;
        public int Id { get; private set; }
        public string CustomerName { get; set; }
        public override string ToString()
        {
            return Id + " " + CustomerName + " " + status + " " + businessUID;
        }
    }
}

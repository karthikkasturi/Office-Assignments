﻿using System;
using System.Threading;
namespace MultitaskingWithKeywordOut
{
    public class Program
    {
        public int k=100;
        public void Caller()
        {
            new ServerClass().OutTestingMethod(out k);
        }

        public void Worker(){
            Thread t1 = new Thread(Caller);
            Thread t2 = new Thread(Caller);
            t1.Name = "First Thread";
            t1.Start();
            Thread.Sleep(1000);
            Console.WriteLine("In " + Thread.CurrentThread.Name + ", after 1sec, k is {0}", k);
            t2.Name = "Second Thread";
            t2.Start();
            Thread.Sleep(11000);
        }

        public static void Main()
        {
            Program programObj = new Program();
            Thread.CurrentThread.Name = "Main Thread";
            programObj.Worker();
            Console.WriteLine("Main finished execution");
            Console.ReadKey();
        }
    }
}

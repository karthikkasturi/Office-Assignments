using System;
using System.Threading;
namespace MultitaskingWithKeywordOut
{
    public class ServerClass
    {
        public void OutTestingMethod(out int k)
        {
            string currentThreadName = Thread.CurrentThread.Name;
            Console.WriteLine(currentThreadName + " invoked." );
            k=5;
            Console.WriteLine(currentThreadName + " is initializing k to " + k );
            Console.WriteLine(currentThreadName + " started to sleep for 3000 mseconds");
            Thread.Sleep(3000);
            if(k!=5)
            {
                Console.WriteLine("The value of k was 5 when " + currentThreadName + " started sleep but now is " + k);
            }
            Console.Write(currentThreadName + " resumed and changed k from " + k);
            k=6;
            Console.WriteLine(" to " + k);
            Console.WriteLine(currentThreadName + ": The instance method called by the worker thread has ended.");
        }
        
    }
}

﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVCGoogleAuth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVCGoogleAuth.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }
        //public async Task<ActionResult> GenerateRoles()
        //{
        //    await Task.Delay(4000);
        //    using (ApplicationDbContext db = new ApplicationDbContext())
        //    {
        //        var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
        //        var adminRole = roleManager.Roles.First(m => m.Name.Equals("Admin"));
        //        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        //        foreach (var user in userManager.Users)
        //        {
        //            if (user.Email.EndsWith("@ggktech.com"))
        //            {
        //                var x = user.Roles.Where(m => m.RoleId.Equals(adminRole.Id)).FirstOrDefault();
        //                if (x == null)
        //                {
        //                    userManager.AddToRole(userId: user.Id, role: "Admin");
        //                }
        //            }
                        
        //        }
        //    }

        //    return View("Index");
        //}
    }
}
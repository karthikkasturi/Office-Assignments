﻿using DataModel;
using DomainModel.DTO;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Security;

namespace GVOne.Controllers
{
    [Authorize(Roles="Admin")]
    public class AccountController : ApiController
    {
        public HttpResponseMessage GetUserDetails(string email, string password)
        {
            AccountRepository repo = new AccountRepository();
            var user = repo.GetUserDetails(email, password);
            string[] roles = repo.GetRoles(user).Select(x => x.Name).ToArray();
            if (user == null)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Invalid email or password.");
            }

            //RequestContext.Principal = new GenericPrincipal(
            //        new GenericIdentity(user.Email, "Form"),
            //        roles
            //    );
            ////FormsAuthentication.SetAuthCookie(user.Email, false);
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }
        public HttpResponseMessage GetSettingsForUser(string id)
        {
            SettingsRepository repo = new SettingsRepository();
            DomainModel.DTO.Settings settings = repo.GetSettings(id);
            if (settings == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, "No data found.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, settings);
            }
        }
        
    }

}

﻿using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GVOne.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AlertsController : ApiController
    {
        AlertRepository repo;
        public AlertsController()
        {
            repo = new AlertRepository();
        }
        public HttpResponseMessage GetUnreadAlertsForUser(string id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, repo.GetUnreadAlerts(id));
        }
        public HttpResponseMessage GetAllAlertsForUser(string id, int offset = 0, int length = 20)
        {
            return Request.CreateResponse(HttpStatusCode.OK, repo.GetAllAlerts(id, offset, length));
        }
    }
}

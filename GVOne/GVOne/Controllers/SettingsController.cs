﻿using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GVOne.Controllers
{
    public class SettingsController : ApiController
    {
        [Authorize(Roles = "Admin")] 
        public HttpResponseMessage GetSettingsForUser(string id)
        {
            SettingsRepository repo = new SettingsRepository();
            var settings = repo.GetSettings(id);
            if (settings == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, "No data found.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, settings);
            }
        }
    }
}

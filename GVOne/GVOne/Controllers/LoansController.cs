﻿using DataModel;
using DomainModel.DTO;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GVOne.Controllers
{
    [Authorize(Roles="Admin")]
    public class LoansController : ApiController
    {
        LoanRepository _repo ;

        public LoansController()
        {
            _repo = new LoanRepository();
        }
        public HttpResponseMessage GetLoansForUser(int offset, int length)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetLoans(offset, length));
        }
        public HttpResponseMessage GetLoanByID(string id)
        {
            LoanDetail loan = _repo.GetLoanByID(id);
            if (loan == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Loan With Given ID");
            }
            return Request.CreateResponse(HttpStatusCode.OK, loan);
        }
        public HttpResponseMessage GetLoanByActualID(string id)
        {
            LoanDetail loan = _repo.GetLoanByActualID(id);
            if (loan == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Loan With Given ID");
            }
            return Request.CreateResponse(HttpStatusCode.OK, loan);
        }
    }
}

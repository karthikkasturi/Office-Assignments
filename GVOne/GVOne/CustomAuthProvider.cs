﻿using Microsoft.Owin.Security.OAuth;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace GVOne
{
    public class CustomAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            await Task.Run(() => { });
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            AccountRepository repo = new AccountRepository();
            var user = repo.GetUserDetails(context.UserName, context.Password);
            if(user != null)
            {
                var roles = repo.GetRoles(user);
                foreach (var role in roles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
                }
                context.Validated(identity);
            }
            else
            {
                context.SetError("Message", "invalid credentials");
            }
            await Task.Run(() => { });
        }
    }
}
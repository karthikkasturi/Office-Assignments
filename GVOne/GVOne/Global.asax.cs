﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Security;


namespace GVOne
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

        }
        
        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        // string username = Request.Cookies[FormsAuthentication.FormsCookieName].Value;
                        string email = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        using (GVOneEntities db = new GVOneEntities())
                        {
                            var roles =  db.tblUser.SingleOrDefault(u => u.Email == email).tblRole;
                            e.User = new System.Security.Principal.GenericPrincipal(
                                                new System.Security.Principal.GenericIdentity(email, "Forms"),
                                                roles.Select(x => x.Name).ToArray()
                                            );
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }
    }
}


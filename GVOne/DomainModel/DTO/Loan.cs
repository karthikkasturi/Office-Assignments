﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.DTO
{
    public class Loan
    {
        public Guid ID { get; set; }
        public string ActualLoanID { get; set; }
        public string BorrowerAddress { get; set; }
        public string BorrowerName { get; set; }
    }
}

﻿namespace DomainModel.DTO
{
    using System;
    

    public partial class Role
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Role()
        {
            this.ID = Guid.NewGuid();
        }

        public System.Guid ID { get; set; }
        public string Name { get; set; }

    }
}
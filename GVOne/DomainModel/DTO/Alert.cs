﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.DTO
{
    public class Alert
    {
        public bool IsRead { get; set; }
        public System.Guid ID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }

    }
}

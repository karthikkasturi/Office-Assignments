﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainModel.DTO
{
    public class LoanDetail
    {
        public LoanDetail()
        {
            this.Borrowers = new HashSet<User>();
        }
        public Guid ID { get; set; }
        public string ActualLoanID{ get; set; }
        public User PrimaryBorrower { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime ModifiedAt { get; set; }
        public IEnumerable<User> Borrowers { get; set; }
        public User Agent { get; set; }
        public LoanStatus Status { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;

namespace DomainModel.DTO
{
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            //this.Roles = new HashSet<Role>();
            this.ID = Guid.NewGuid();
        }

        public System.Guid ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        //public string Password { get; set; }
        public string Address { get; set; }
        //public bool IsActive { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Role> Roles { get; set; }
    }
}
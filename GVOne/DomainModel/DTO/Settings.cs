﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.DTO
{
    public class Settings
    {
        public System.Guid ID { get; set; }
        public bool IsAllSettingsEnabled { get; set; }
        public bool IsPushNotificationsEnabled { get; set; }
        public bool IsAlertsEnabled { get; set; }
    }
}

﻿namespace DomainModel.DTO
{
    using System;
    using System.Collections.Generic;

    public partial class LoanStatus
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LoanStatus()
        {
            this.ID = Guid.NewGuid();
        }

        public System.Guid ID { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public bool IsActive { get; set; }

        
    }
}
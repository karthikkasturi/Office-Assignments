﻿using DataModel;
using DomainModel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class AlertRepository
    {
        public IEnumerable<Alert> GetUnreadAlerts(string userID)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblUserAlert.Where(x => x.tblUser.tblUserID.ToString().Equals(userID) && x.tblAlert.IsActive && !x.IsDeleted && !x.IsRead).Select(
                    x => new Alert
                    {
                        IsRead = x.IsRead,
                        IsActive = true,
                        CreatedAt = x.tblAlert.CreatedAt,
                        Description = x.tblAlert.Description,
                        ID = x.tblAlert.tblAlertID,
                        Subject = x.tblAlert.Subject
                    }).ToList();
            }

        }
        public IEnumerable<Alert> GetAllAlerts(string userID, int offset, int length)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblUserAlert.Where(x => x.tblUser.tblUserID.ToString().Equals(userID) && x.tblAlert.IsActive && !x.IsDeleted)
                    .OrderByDescending(x => x.tblAlert.CreatedAt).Skip(offset).Take(length).Select(
                    x => new Alert
                    {
                        IsRead = x.IsRead,
                        IsActive = true,
                        CreatedAt = x.tblAlert.CreatedAt,
                        Description = x.tblAlert.Description,
                        ID = x.tblAlert.tblAlertID,
                        Subject = x.tblAlert.Subject
                    }).ToList();
            }

        }
    }
}

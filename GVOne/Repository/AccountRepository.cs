﻿using DataModel;
using DomainModel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class AccountRepository
    {
        public User GetUserDetails(string email, string password)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblUser.Where(x => x.Email.Equals(email) && x.Password != null && x.Password.Equals(password))
                    .Select(x => new User
                    {
                        Address = x.Address,
                        Email = x.Email,
                        ID = x.tblUserID,
                        Mobile = x.Mobile,
                        Name = x.Name
                    }).FirstOrDefault();
            }   
        }
        public IEnumerable<Role> GetRoles(User user)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblUser.Where(x => x.tblUserID.ToString().Equals(user.ID.ToString())).FirstOrDefault().tblRole
                    .Select(
                        x => new Role {
                            ID = x.tblRoleID,
                            Name = x.Name
                        }
                    ).ToList();
            }
        }
    }
}

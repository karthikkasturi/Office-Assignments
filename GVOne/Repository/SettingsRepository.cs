﻿using DataModel;
using DomainModel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class SettingsRepository
    {
        public Settings GetSettings(string userid) {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblSetting.Where(x => x.tblUserID.ToString().Equals(userid))
                    .Select(x => new Settings
                    {
                        ID = x.tblSettingID,
                        IsAlertsEnabled = x.IsAlertsEnabled,
                        IsAllSettingsEnabled = x.IsAllSettingsEnabled,
                        IsPushNotificationsEnabled = x.IsPushNotificationsEnabled
                    }).FirstOrDefault();
            }
        }
    }
}

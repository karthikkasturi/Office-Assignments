﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel;
using DomainModel.DTO;
namespace Repository
{
    public class LoanRepository
    {
        public IEnumerable<Loan> GetLoans(int offset = 0, int length = 20)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                
                return db.tblLoan.Where(x => x.IsActive).OrderByDescending(x => x.CreatedAt).Skip(offset).Take(length).Select(
                    x => new Loan {
                        ID = x.tblLoanID,
                        ActualLoanID = x.ActualLoanID,
                        BorrowerAddress = x.tblUser.Address,
                        BorrowerName = x. tblUser.Name
                    }).ToList();
            }

        }
        public LoanDetail GetLoanByID(string loanID)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblLoan.Where(x => x.IsActive && x.tblLoanID.ToString().Equals(loanID)).Select(
                    x => new LoanDetail
                    {
                        ID = x.tblLoanID,
                        ActualLoanID = x.ActualLoanID,
                        Agent = x.tblUserLoan.Where(y => y.tblRole.Name.Equals("Agent")).Select(
                                y => new User
                                {
                                    Address = y.tblUser.Address,
                                    Email = y.tblUser.Email,
                                    Mobile = y.tblUser.Mobile,
                                    ID = y.tblUserID,
                                    Name = y.tblUser.Name
                                }).FirstOrDefault(),
                        PrimaryBorrower = new User
                        {
                            Address = x.tblUser.Address,
                            Email = x.tblUser.Email,
                            Mobile = x.tblUser.Mobile,
                            ID = x.tblUser.tblUserID,
                            Name = x.tblUser.Name
                        },
                        CreatedAt = x.CreatedAt,
                        ModifiedAt = x.ModifiedAt,
                        Status = new LoanStatus
                        {
                            ID = x.tblLoanStatus.tblLoanStatusID,
                            Description = x.tblLoanStatus.Description,
                            Label = x.tblLoanStatus.Label
                        },
                        Borrowers = x.tblUserLoan.Where(y => y.tblRole.Name.Equals("Borrower")).Select(
                                y => new User
                                {
                                    Address = y.tblUser.Address,
                                    Email = y.tblUser.Email,
                                    Mobile = y.tblUser.Mobile,
                                    ID = y.tblUserID,
                                    Name = y.tblUser.Name
                                }
                            )
                    }).FirstOrDefault();
            }
        }
        public LoanDetail GetLoanByActualID(string loanID)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                return db.tblLoan.Where(x => x.IsActive && x.ActualLoanID.Equals(loanID)).Select(
                    x => new LoanDetail
                    {
                        ID = x.tblLoanID,
                        ActualLoanID = x.ActualLoanID,
                        Agent = x.tblUserLoan.Where(y => y.tblRole.Name.Equals("Agent")).Select(
                                y => new User
                                {
                                    Address = y.tblUser.Address,
                                    Email = y.tblUser.Email,
                                    Mobile = y.tblUser.Mobile,
                                    ID = y.tblUserID,
                                    Name = y.tblUser.Name
                                }).FirstOrDefault(),
                        PrimaryBorrower = new User
                        {
                            Address = x.tblUser.Address,
                            Email = x.tblUser.Email,
                            Mobile = x.tblUser.Mobile,
                            ID = x.tblUser.tblUserID,
                            Name = x.tblUser.Name
                        },
                        CreatedAt = x.CreatedAt,
                        ModifiedAt = x.ModifiedAt,
                        Status = new LoanStatus
                        {
                            ID = x.tblLoanStatus.tblLoanStatusID,
                            Description = x.tblLoanStatus.Description,
                            Label = x.tblLoanStatus.Label
                        },
                        Borrowers = x.tblUserLoan.Where(y => y.tblRole.Name.Equals("Borrower")).Select(
                                y => new User
                                {
                                    Address = y.tblUser.Address,
                                    Email = y.tblUser.Email,
                                    Mobile = y.tblUser.Mobile,
                                    ID = y.tblUserID,
                                    Name = y.tblUser.Name
                                }
                            )
                    }).FirstOrDefault();
            }
        }
        public bool IsAdmin(string userid)
        {
            using (GVOneEntities db = new GVOneEntities())
            {
                var user = db.tblUser.FirstOrDefault(x => x.tblUserID.ToString().Equals(userid));
                if (user == null)
                {
                    return false;
                }
                var adminRole = user.tblRole.FirstOrDefault(x => x.Name.Equals("Admin"));
                if (adminRole == null)
                {
                    return false;
                }
                return true;
            }
        }
        
    }
}

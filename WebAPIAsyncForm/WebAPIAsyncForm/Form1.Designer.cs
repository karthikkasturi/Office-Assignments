﻿namespace WebAPIAsyncForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GetStudentsLabel = new System.Windows.Forms.Label();
            this.GetStudentsByIDLabel = new System.Windows.Forms.Label();
            this.GetGroupsButton = new System.Windows.Forms.Button();
            this.GetGroupsByIDLabel = new System.Windows.Forms.Label();
            this.GetGroupsLabel = new System.Windows.Forms.Label();
            this.GetGroupsByIDButton = new System.Windows.Forms.Button();
            this.GetStudentsByIDButton = new System.Windows.Forms.Button();
            this.GetStudentsButton = new System.Windows.Forms.Button();
            this.RunTimeLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // GetStudentsLabel
            // 
            this.GetStudentsLabel.AutoSize = true;
            this.GetStudentsLabel.Location = new System.Drawing.Point(134, 40);
            this.GetStudentsLabel.Name = "GetStudentsLabel";
            this.GetStudentsLabel.Size = new System.Drawing.Size(92, 13);
            this.GetStudentsLabel.TabIndex = 1;
            this.GetStudentsLabel.Text = "GetStudentsLabel";
            // 
            // GetStudentsByIDLabel
            // 
            this.GetStudentsByIDLabel.AutoSize = true;
            this.GetStudentsByIDLabel.Location = new System.Drawing.Point(134, 80);
            this.GetStudentsByIDLabel.Name = "GetStudentsByIDLabel";
            this.GetStudentsByIDLabel.Size = new System.Drawing.Size(115, 13);
            this.GetStudentsByIDLabel.TabIndex = 3;
            this.GetStudentsByIDLabel.Text = "GetStudentsByIDLabel";
            // 
            // GetGroupsButton
            // 
            this.GetGroupsButton.Location = new System.Drawing.Point(12, 115);
            this.GetGroupsButton.Name = "GetGroupsButton";
            this.GetGroupsButton.Size = new System.Drawing.Size(116, 23);
            this.GetGroupsButton.TabIndex = 5;
            this.GetGroupsButton.Text = "GetGroups";
            this.GetGroupsButton.UseVisualStyleBackColor = true;
            this.GetGroupsButton.Click += new System.EventHandler(this.GetGroupsButton_Click);
            // 
            // GetGroupsByIDLabel
            // 
            this.GetGroupsByIDLabel.AutoSize = true;
            this.GetGroupsByIDLabel.Location = new System.Drawing.Point(134, 159);
            this.GetGroupsByIDLabel.Name = "GetGroupsByIDLabel";
            this.GetGroupsByIDLabel.Size = new System.Drawing.Size(107, 13);
            this.GetGroupsByIDLabel.TabIndex = 7;
            this.GetGroupsByIDLabel.Text = "GetGroupsByIDLabel";
            // 
            // GetGroupsLabel
            // 
            this.GetGroupsLabel.AutoSize = true;
            this.GetGroupsLabel.Location = new System.Drawing.Point(134, 120);
            this.GetGroupsLabel.Name = "GetGroupsLabel";
            this.GetGroupsLabel.Size = new System.Drawing.Size(84, 13);
            this.GetGroupsLabel.TabIndex = 8;
            this.GetGroupsLabel.Text = "GetGroupsLabel";
            // 
            // GetGroupsByIDButton
            // 
            this.GetGroupsByIDButton.Location = new System.Drawing.Point(12, 154);
            this.GetGroupsByIDButton.Name = "GetGroupsByIDButton";
            this.GetGroupsByIDButton.Size = new System.Drawing.Size(116, 23);
            this.GetGroupsByIDButton.TabIndex = 9;
            this.GetGroupsByIDButton.Text = "GetGroupsByID505";
            this.GetGroupsByIDButton.UseVisualStyleBackColor = true;
            this.GetGroupsByIDButton.Click += new System.EventHandler(this.GetGroupsByIDButton_Click);
            // 
            // GetStudentsByIDButton
            // 
            this.GetStudentsByIDButton.Location = new System.Drawing.Point(12, 75);
            this.GetStudentsByIDButton.Name = "GetStudentsByIDButton";
            this.GetStudentsByIDButton.Size = new System.Drawing.Size(116, 23);
            this.GetStudentsByIDButton.TabIndex = 11;
            this.GetStudentsByIDButton.Text = "GetStudentsByID505";
            this.GetStudentsByIDButton.UseVisualStyleBackColor = true;
            this.GetStudentsByIDButton.Click += new System.EventHandler(this.GetStudentsByIDButton_Click);
            // 
            // GetStudentsButton
            // 
            this.GetStudentsButton.Location = new System.Drawing.Point(12, 35);
            this.GetStudentsButton.Name = "GetStudentsButton";
            this.GetStudentsButton.Size = new System.Drawing.Size(116, 23);
            this.GetStudentsButton.TabIndex = 10;
            this.GetStudentsButton.Text = "GetStudents";
            this.GetStudentsButton.UseVisualStyleBackColor = true;
            this.GetStudentsButton.Click += new System.EventHandler(this.GetStudentsButton_Click);
            // 
            // RunTimeLabel
            // 
            this.RunTimeLabel.AutoSize = true;
            this.RunTimeLabel.Location = new System.Drawing.Point(214, 255);
            this.RunTimeLabel.Name = "RunTimeLabel";
            this.RunTimeLabel.Size = new System.Drawing.Size(76, 13);
            this.RunTimeLabel.TabIndex = 12;
            this.RunTimeLabel.Text = "RunTimeLabel";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 303);
            this.Controls.Add(this.RunTimeLabel);
            this.Controls.Add(this.GetStudentsButton);
            this.Controls.Add(this.GetStudentsByIDButton);
            this.Controls.Add(this.GetGroupsByIDButton);
            this.Controls.Add(this.GetGroupsLabel);
            this.Controls.Add(this.GetGroupsByIDLabel);
            this.Controls.Add(this.GetGroupsButton);
            this.Controls.Add(this.GetStudentsByIDLabel);
            this.Controls.Add(this.GetStudentsLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion
        private System.Windows.Forms.Label GetStudentsLabel;
        private System.Windows.Forms.Label GetStudentsByIDLabel;
        private System.Windows.Forms.Button GetGroupsButton;
        private System.Windows.Forms.Label GetGroupsByIDLabel;
        private System.Windows.Forms.Label GetGroupsLabel;
        private System.Windows.Forms.Button GetGroupsByIDButton;
        private System.Windows.Forms.Button GetStudentsByIDButton;
        private System.Windows.Forms.Button GetStudentsButton;
        private System.Windows.Forms.Label RunTimeLabel;
        private System.Windows.Forms.Timer timer1;
    }
}


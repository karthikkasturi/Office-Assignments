﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Windows.Forms;
using WebAPIPostGetController;
namespace WebAPIAsyncForm
{
    public partial class Form1 : Form
    {
        Controller _controller = new Controller("http://localhost:53864/api/values/");

        public Form1()
        {
            InitializeComponent();
            RunTimeLabel.Text = DateTime.Now.ToString();
            timer1.Interval = 100;
            timer1.Start();
        }
       
        private async void GetStudentsButton_Click(object sender, EventArgs e)
        {
            GetStudentsLabel.Text = "Waiting For Data";
            IEnumerable<Student> students;
            students = await _controller.Get<Student>();
            string op = string.Empty;
            foreach (var student in students)
            {
                op += student.StudentId + " " + student.StudentName + "; ";
            }
            GetStudentsLabel.Text = op;
        }

        private async void GetStudentsByIDButton_Click(object sender, EventArgs e)
        {
            GetStudentsByIDLabel.Text = "Waiting For Data";
            IEnumerable<Student> students;
            ControllerGetArgs getArgs = new ControllerGetArgs();
            getArgs.Add("id", 505);
            students = await _controller.Get<Student>(getArgs);
            string op = string.Empty;
            foreach (var student in students)
            {
                op += student.StudentId + " " + student.StudentName + "; ";
            }
            GetStudentsByIDLabel.Text = op;
        }

        

        private async void GetGroupsButton_Click(object sender, EventArgs e)
        {
            GetGroupsLabel.Text = "Waiting For Data";
            IEnumerable<Group> groups;
            groups = await _controller.Get<Group>();
            string op = string.Empty;
            foreach (var group in groups)
            {
                op += group.GroupId + " " + group.GroupName + "; ";
            }
            GetGroupsLabel.Text = op;
        }

        private async void GetGroupsByIDButton_Click(object sender, EventArgs e)
        {
            GetGroupsByIDLabel.Text = "Waiting For Data";
            IEnumerable<Group> groups;
            ControllerGetArgs getArgs = new ControllerGetArgs();
            getArgs.Add("id", 505);
            groups = await _controller.Get<Group>(getArgs);
            string op = string.Empty;
            foreach (var group in groups)
            {
                op += group.GroupId + " " + group.GroupName + "; ";
            }
            GetGroupsByIDLabel.Text = op;
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RunTimeLabel.Text = DateTime.Now.ToString();
            
        }

        //private async void RunTimeLabel_Click(object sender, EventArgs e)
        //{
        //    timer1_Tick(null, null);
        //}
    }
}

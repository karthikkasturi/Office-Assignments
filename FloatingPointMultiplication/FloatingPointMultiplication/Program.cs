﻿using System;
namespace FloatingPointMultiplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the first value: ");
            BinaryFloat operand1 = new BinaryFloat(Convert.ToDouble(Console.ReadLine()));
            Console.Write("Enter the second value: ");
            BinaryFloat operand2 = new BinaryFloat(Convert.ToDouble(Console.ReadLine()));
            BinaryFloat result = new BinaryFloatMultiplication().Multiply(operand1, operand2);
            Console.WriteLine(result);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}

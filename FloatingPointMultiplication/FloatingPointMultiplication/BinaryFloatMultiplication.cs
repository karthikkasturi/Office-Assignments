namespace FloatingPointMultiplication
{
    internal class BinaryFloatMultiplication
    {
        /// <summary>
        /// Implement Floating Point Multiplication for current instance and an operand.
        /// </summary>
        /// <param name="operand"></param>
        /// <returns> A new BinaryFloat object which is the result of multiplication. </returns>
        public BinaryFloat Multiply(BinaryFloat operand1, BinaryFloat operand2) 
        {
            int[] firstBinary = operand1.HiddenBitRepresentation;
            int[] secondBinary = operand2.HiddenBitRepresentation;
            int[] resultingBinary = new int[64];
            int resultingBinaryLength = 64;
            // Multiply the values
            for (int i = 0; i < 24; i++)
            {
                int multiplier = secondBinary[23 - i];
                if(multiplier == 0) continue;
                int carry = 0, resultFlight = 0, binsum = 0;
                for (int j = 0; j < 24; j++)
                {
                    resultFlight = resultingBinaryLength - j - i - 1;
                    binsum = firstBinary[24 - j - 1] + resultingBinary[resultFlight] + carry;
                    resultingBinary[resultFlight] = binsum % 2;
                    carry = binsum / 2;
                }
                if (resultFlight > 0) //to avoid zero error
                    resultingBinary[resultFlight - 1] = carry;
            }
            // Decimal point must ideally be 23+23 = 46 digits from last (64-46=18).
            // Exp is exp of 1 + exp of 2.
            // Adjust exp furthur by finding how far the first 1 is from the 46th digit.
            // Add mantissa from adjusted decimal to the res binary.
            // Set Exponent in the finalResult array
            int[] finalResult = new int[32];
            int expAdjustment = 0;
            while(resultingBinary[17-expAdjustment]!=1) expAdjustment++;
            for(int i = 1; i<=23; i++ )
            {
                finalResult[i + 8] = resultingBinary[17-expAdjustment+i];
            }
            finalResult[0] = operand1.Sign ^ operand2.Sign; 
            int exp = operand1.Exponent + operand2.Exponent + expAdjustment;
            operand1.SetExponentForArray(exp, finalResult);
            return new BinaryFloat(finalResult);
        }
    }
}

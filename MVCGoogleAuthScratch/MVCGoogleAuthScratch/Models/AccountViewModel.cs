﻿using System.ComponentModel.DataAnnotations;
namespace MVCGoogleAuthScratch.Models
{
    public class ExternalLoginRegisterViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Provider { get; internal set; }
    }
}
﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MVCGoogleAuthScratch.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;

namespace MVCGoogleAuthScratch.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [AllowAnonymous]
        public ActionResult GoogleLogIn()
        {
            return new ChallengeResult("Google", Url.Action("GoogleCallback", "Account", new { ReturnUrl = "/Home/Index" }));            
        }
        [AllowAnonymous]
        public ActionResult GoogleCallback()
        {
            var loginInfo = //HttpContext.GetOwinContext().Authentication.GetExternalLoginInfo();//
                ExtractIdentityAF();
            if (loginInfo == null)
            {
                return View("LoginFailure");
            }
            else
            {
                return LoginWithProvider(loginInfo);
            }
        }
        private ActionResult LoginWithProvider(ExternalLoginInfo info)
        {
            using (GAuthEntities db = new GAuthEntities())
            {
                var userLogin = db.UserLogins.FirstOrDefault(x =>
                   x.Provider.Equals(info.Login.LoginProvider) &&
                   x.LoginID.Equals(info.Login.ProviderKey)
                    );
                if (userLogin == null)
                {
                    return View("ExternalLoginRegister"
                        , new ExternalLoginRegisterViewModel
                        {
                            Name = info.ExternalIdentity.Name,
                            Email = info.Email,
                            Provider = info.Login.LoginProvider
                        });
                }
                else
                {
                    var user = db.Users.FirstOrDefault(x => x.ID.ToString().Equals(userLogin.UserID.ToString()));
                    if (user == null)
                    {
                        return View("LoginFailure");
                    }
                    else
                    {
                        ClaimsIdentity claims = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.NameIdentifier, user.Email),
                            new Claim(ClaimTypes.Name, user.Name),
                            new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                                    "ASP.NET Identity",
                                    "http://www.w3.org/2001/XMLSchema#string"),

                        }, CookieAuthenticationDefaults.AuthenticationType);
                        foreach (var role in user.Roles)
                        {
                            claims.AddClaim(new Claim(ClaimTypes.Role, role.Name));
                        }
                        HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = false }, claims);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            //return View("LoginFailure");
        }
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ExternalLoginRegister(ExternalLoginRegisterViewModel model)
        {
            //if (User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Index", "Manage");
            //}
            if (ModelState.IsValid)
            {
                ExternalLoginInfo info =// HttpContext.GetOwinContext().Authentication.GetExternalLoginInfo();//
                    ExtractIdentityAF();
                if (info == null)
                {
                    return View("LoginFailure");
                }
                //var user = new User { Username = model.Name, Email = model.Email, Name = model.Name };
                //var result = await UserManager.CreateAsync(user);

                using (GAuthEntities db = new GAuthEntities())
                {
                    var user = new User { ID = Guid.NewGuid(), Username = info.DefaultUserName, Email = model.Email, Name = model.Name };
                    user.UserLogins.Add(new UserLogin { UserID = user.ID, Provider = info.Login.LoginProvider, LoginID = info.Login.ProviderKey });
                    var adminRole = db.Roles.First(x => x.Name.Equals("Admin"));
                    if (user.Email.EndsWith("@ggktech.com"))
                        adminRole.Users.Add(user);
                    db.Roles.First(x => x.Name.Equals("User")).Users.Add(user);
                    db.SaveChanges();
                }
                return LoginWithProvider(info);
            }
            return View(model);
        }

        private ExternalLoginInfo ExtractIdentityAF()
        {
            var iden = HttpContext.User.Identity as ClaimsIdentity;
            string username = iden.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name)).Value;
            username = username.Replace(" ", string.Empty);
            string url = iden.Claims.FirstOrDefault(x => x.Issuer.Equals("Google") && (x.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"))).Value;
            string loginId = url.Substring(url.LastIndexOf('/')+1);
            ExternalLoginInfo info = new ExternalLoginInfo()
            {
                DefaultUserName = username,
                Email = iden.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Email)).Value,
                ExternalIdentity = iden,
                Login = new UserLoginInfo("Google", loginId)
            };
            return info;
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return RedirectToAction("Index", "Home");
        }
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
    }
}
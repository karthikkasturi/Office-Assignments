﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Google;
using MVCGoogleAuthScratch.Models;

[assembly: OwinStartup(typeof(MVCGoogleAuthScratch.Startup))]
namespace MVCGoogleAuthScratch
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                LoginPath = new PathString("/Account/Login"),
                SlidingExpiration = true
                //Provider = new CookieAuthenticationProvider
                //{
                //    // Enables the application to validate the security stamp when the user logs in.
                //    // This is a security feature which is used when you change a password or add an external login to your account.  
                //    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                //       validateInterval: TimeSpan.FromMinutes(30),
                //       regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                //}
            });
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "492007039950-pkvfpoln5cbuffb9ogugds8tdc3g097i.apps.googleusercontent.com",
                ClientSecret = "jStx9Y7Jy-c8dxBSesSg8onL",
                //CallbackPath = new PathString("/Account/GoogleCallback")
            });
            using (GAuthEntities db = new GAuthEntities())
            {
                var adminRole = db.Roles.FirstOrDefault(x => x.Name.Equals("Admin"));
                if (adminRole == null)
                {
                    adminRole = new Role { ID = Guid.NewGuid(), Name = "Admin" };
                    db.Roles.Add(adminRole);
                }
                var userRole = db.Roles.FirstOrDefault(x => x.Name.Equals("User"));
                if (userRole == null)
                {
                    userRole = new Role { ID = Guid.NewGuid(), Name = "User" };
                    db.Roles.Add(userRole);
                }
                db.SaveChanges();
            }
        }
    }
}
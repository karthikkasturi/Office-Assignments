﻿create table [User] (
	ID uniqueidentifier default newid() primary key,
	Name varchar(40),
	Username varchar(40),
	Email varchar(40)
)



create table [Role] (
	ID uniqueidentifier default newid() primary key,
	Name varchar(10)
)

create table [UserRole](
	RoleID uniqueidentifier foreign key references [Role](Id),
	UserID uniqueidentifier foreign key references [User](Id),
	constraint pk_userrole primary key(RoleId, UserId)
)

create table [UserLogin](
	[Provider] varchar(20),
	[LoginID] varchar(150),
	[UserID] uniqueidentifier foreign key references [User](Id),
	constraint pk_userlogin primary key([provider], [LoginId])
)
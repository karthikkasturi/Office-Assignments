﻿using System;
using System.Threading;

namespace StaticIncrementer
{
    class Program
    {
        static int x = 1;
        static void Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(x++);
                Thread.Sleep(500);
            }
        }
    }
}

namespace FloatingPointAddition
{
    internal class BinaryFloatAddition
    {
        /// <summary>
        /// Checks the variant of addition methodology to be performed based on sign and value and calls the appropriate functionality.
        /// </summary>
        /// <param name="operand"></param>
        /// <returns>The Result of addition</returns>
        public BinaryFloat CallAdditionByCase(BinaryFloat operand1, BinaryFloat operand2)
        {
            double floatValue1 = operand1.FloatValue, floatValue2 = operand2.FloatValue;
            if(floatValue1 == 0.0)
            {
                return new BinaryFloat(floatValue2);
            } 
            if(floatValue2 == 0.0) 
            {
                return new BinaryFloat(floatValue1);
            }
            if(floatValue1 > 0 && floatValue2 > 0)
            {
                return ( (floatValue1 > floatValue2) ? Addition(operand1, operand2) : Addition(operand2, operand1) );
            }
            else if(floatValue1 > 0 && floatValue2 < 0)
            {
                return ( (floatValue1 > -floatValue2) ? Subtraction(operand1, operand2) : Subtraction(operand2, operand1).ChangeSign() );
            }
            else if(floatValue1 < 0 && floatValue2 > 0)
            {
                return ( (-floatValue1 > floatValue2) ? Subtraction(operand1, operand2).ChangeSign() : Subtraction(operand2, operand1) );
            }
            else if(floatValue1 < 0 && floatValue2 < 0)
            {
                return ( (-floatValue1 > -floatValue2) ? Addition(operand1, operand2).ChangeSign() : Addition(operand2, operand1).ChangeSign() );
            }
            else return null;
        }
        /// <summary>
        /// Perform Floating point addition on two operands.
        /// </summary>
        /// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns> The Addition result as a BinaryFloat object. </returns>
        private BinaryFloat Addition(BinaryFloat operand1, BinaryFloat operand2)
        {
            int[] result = new int[32];
            int exp1 = operand1.Exponent;
            int exp2 = operand2.Exponent;
            // Get the hidden bit representations(24 bit array).
            int[] firstBinary = operand1.HiddenBitRepresentation;
            int[] secondBinary = operand2.HiddenBitRepresentation;
            // Right Shift the secondBinary so that its exponent matches the firstBinary's exponent.
            ShiftBinaryByNBits(secondBinary, exp1-exp2);
            // Add the binaries
            int[] summedBinary = AddBinaries(firstBinary, secondBinary);
            // If the carry bit (summedBinary[0]) is one, exponent is to be increased by one.
            operand1.SetExponentForArray(exp1 + summedBinary[0], result);
            // Copy the summedBinary as our final result mantissa
            for(int i = 0; i < 23; i++ )
            {
                result[9 + i] = summedBinary[2 - summedBinary[0] + i];
            }
            // Return a BinaryFloat object initialized with the array rep of the result.
            return new BinaryFloat(result);
        }
        /// <summary>
        /// Perform Floating Point Subtraction on two operands.
        /// </summary>
        /// <param name="operand1"></param>
        /// <param name="operand2"></param>
        /// <returns> The subtraction result as a BinaryFloat object. </returns>
        private BinaryFloat Subtraction(BinaryFloat operand1, BinaryFloat operand2)
        {
            int[] result = new int[32];
            int exp1 = operand1.Exponent;
            int exp2 = operand2.Exponent;
            // Get the hidden bit representations(24 bit array).
            int[] firstBinary = operand1.HiddenBitRepresentation;
            int[] secondBinary = operand2.HiddenBitRepresentation;
            // Right Shift the secondBinary so that its exponent matches the firstBinary's exponent.
            ShiftBinaryByNBits(secondBinary, exp1-exp2);
            // Subtraction is achieved by performing 2's Compliment on second array and adding the arrays.
            int carry = 1, bitsum;
            for(int i = 23; i >= 0; i--)
            {
                secondBinary[i] = secondBinary[i] ^ 1;
                bitsum = secondBinary[i] + carry; 
                secondBinary[i] = bitsum % 2;
                carry = bitsum / 2;
            }
            int[] summedBinary = AddBinaries(firstBinary, secondBinary);
            // The Carry Bit (summedBinary[0]) is to be discarded. 
            summedBinary[0] = 0;
            // We need to find where 1 occurs in the sum and adjust mantissa and exponent accordingly.
            int finder ;
            for(finder = 1; finder<25 && summedBinary[finder] == 0; finder++)
            // If 1 isnt found (finder=25), the result of subtraction is zero.
            if(finder == 25) 
            {
                return new BinaryFloat(); // Returns a BinaryFloat object with zero value.
            }
            operand1.SetExponentForArray(exp1 - finder + 1, result);
            for(int i = 0; i < 24 - finder; i++)
            {
                result[9+i] = summedBinary[finder + 1 + i];
            }
            return new BinaryFloat(result);
        }
        /// <summary>
        /// Shift a Binary array by n bits to the right.
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="n"></param>
        private void ShiftBinaryByNBits(int[] binary, int n)
        {
            int binaryLength = binary.Length;
            for(int i=0; i<n; i++)
            {
                for(int j=binaryLength-1; j>=1; j--)
                {
                    binary[j] = binary[j-1];
                }
                binary[0] = 0;
            }
        }
        /// <summary>
        /// Performs addition on two binaries.
        /// </summary>
        /// <param name="binary1"></param>
        /// <param name="binary2"></param>
        /// <returns> An int[] object which contains the summed result of the two binaries </returns>
        private int[] AddBinaries(int[] binary1, int[] binary2)
        {
            int[] res = new int[binary1.Length+1];
            int binaryLength = binary1.Length, carry = 0, bitsum = 0;
            for(int i=binaryLength-1; i>=0; i--)
            {
                bitsum = binary1[i] + binary2[i] + carry;
                res[i+1] = bitsum % 2;
                carry = bitsum / 2;
            }
            if(binaryLength > 0) res[0] = carry;
            return res;
        }
    }
}
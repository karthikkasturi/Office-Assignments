namespace FloatingPointAddition
{
    internal class BinaryFloat
    {
        private double value;
        private int[] wholeBinary = new int[32];
        /// <summary>
        /// Constructor overload with initialization from an int[] array which is a floating point representation.
        /// </summary>
        /// <param name="binary"></param>
        public BinaryFloat(int[] binary)
        {
            wholeBinary = (int[])binary.Clone();
            GenerateValueFromStoredBinary();
        }
        /// <summary>
        /// Constructor overload with initialization from a double value.
        /// </summary>
        /// <param name="value"></param>
        public BinaryFloat(double value)
        {
            this.value = value;
            if (value < 0)
            {
                value = value * -1;
                wholeBinary[0] = 1;
            }
            double mantissa = value % 1.0;
            double characteristic = value - mantissa;
            GenerateBinary(characteristic, mantissa);
        }
        /// <summary>
        /// Default Constructor, initializes with value set as zero.
        /// </summary>
        public BinaryFloat()
        {
            value = 0;
            GenerateBinary(0,0);
        }
        /// <summary>
        /// Generate the float value from the floating point representing binary stored in object.
        /// </summary>
        private void GenerateValueFromStoredBinary()
        {
            value = 0.0;
            int[] hiddenBitRepresentation = this.HiddenBitRepresentation;
            int exp = this.Exponent;
            double multiplier = 1;
            for(int i = exp; i>=0; i--)
            {
                value += HiddenBitRepresentation[i]*multiplier;
                multiplier*=2;
            }
            multiplier = 0.5;
            for(int i=exp+1; i<24;i++)
            {
                value+= HiddenBitRepresentation[i]*multiplier;
                multiplier = multiplier/2;
            }
            value = value * (this.Sign==1?-1:1);
        }
        /// <summary>
        /// Generate the floating point binary based on characteristic and mantissa.
        /// </summary>
        /// <param name="characteristic"></param>
        /// <param name="mantissa"></param>
        private void GenerateBinary(double characteristic,double mantissa)
        {
            int[] characteristicInBinary = GenerateCharacteristicBinary(characteristic);
            int[] mantissaInBinary = GenerateMantissaBinary(mantissa);
            int[] mergedBinary = new int[279];
            for (int i = 0; i < 279; i++)
            {
                mergedBinary[i] = i<128?characteristicInBinary[i<128?i:0]:mantissaInBinary[i-128];
            }
            int exp = 0;
            for( ; exp<256 && mergedBinary[exp]==0; exp++);
            // If exp==256, it means that the value is zero
            if(exp==256) exp = 127;
            SetExponentForArray(127-exp, wholeBinary);
            for(int i = 1; i<=23; i++ )
            {
                wholeBinary[i + 8] = mergedBinary[exp+i];
            }
        }
        /// <summary>
        /// Generate Binary representation of the characteristic.
        /// </summary>
        /// <param name="characteristic"></param>
        /// <returns> An int[] object which is the binary representation of characteristic. </returns>
        private int[] GenerateCharacteristicBinary(double characteristic)
        {
            int[] characteristicInBinary = new int[128], characteristicInBinaryTemp = new int[128];
            int characteristicLength = 0;
            while (characteristic > 0 && characteristicLength < 128)
            {
                characteristicInBinaryTemp[characteristicLength++] = (int)characteristic % 2;
                characteristic = characteristic / 2;
                characteristic = characteristic - (characteristic%1.0);
            }
            for (int i = 0; i < characteristicLength; i++)
            {
                characteristicInBinary[128 - characteristicLength + i ] = characteristicInBinaryTemp[characteristicLength - i - 1];
            }
            return characteristicInBinary;
        }
        /// <summary>
        /// Generate binary representation of the mantissa part.
        /// </summary>
        /// <param name="mantissa"></param>
        /// <returns> An int[] object which is the binary representation of mantissa. </returns>
        private int[] GenerateMantissaBinary(double mantissa)
        {
            int mantissaLength = 0;
            int[] mantissaInBinary = new int[151];
            while (mantissa > 0 && mantissaLength < 151 )
            {
                mantissa = mantissa * 2.0;
                mantissaInBinary[mantissaLength++] = (int)mantissa / 1;
                mantissa = mantissa % 1.0;
            }
            return mantissaInBinary;
        }
        /// <summary>
        /// Set exponent in the Floating point representation array.
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="binary"></param>
        public void SetExponentForArray(int exp, int[] binary)
        {
            exp = 127 + exp;
            for(int i = 8;i>=1;i--)
            {
                binary[i] = exp%2;
                exp = exp/2;
            } 
        }
		/// <summary>
        /// Changes the sign bit of the floating point representation.
        /// </summary>
        /// <returns> The object itself. </returns>
        public BinaryFloat ChangeSign()
        {
            wholeBinary[0] = wholeBinary[0] ^ 1;
            if(this.value!=0) GenerateValueFromStoredBinary();
            return this;
        }
        /// <summary>
        /// ToString() implementation for the BinaryFloat class objects.
        /// </summary>
        /// <returns> The value in string format. </returns>
        override public string ToString()
        {
            return (value).ToString();
        }
        public int Exponent
        {
                get{
                    int k=0,mul=1;
                    for(int i=8;i>0;i--) {
                        k+=wholeBinary[i]*mul;
                        mul*=2;
                    }
                    return k-127;
                }

        }
        public int[] HiddenBitRepresentation
        {
            get{
                int[] hbr = new int[24];
                hbr[0] = 1;
                for(int i = 0; i<23; i++)
                {
                    hbr[1 + i] = wholeBinary[9 + i];
                }
                return hbr;
            }
        }
        public int Sign
        {
            get { return wholeBinary[0]; }
        }
        public float FloatValue 
        {
             get { return (float)value; } 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Reflection;

namespace WebAPIPostGetController
{
    public class ControllerGetArgs
    {
        //private Dictionary<PropertyInfo, object> _args = new Dictionary<PropertyInfo, object>();
        private Dictionary<string, object> _args = new Dictionary<string, object>();
        public void Add(string propertyName, object value)
        {
            _args.Add(propertyName, value);
        }
        public void Clear()
        {
            _args.Clear();
        }
        public Dictionary<string, object> Args
        {
            get { return _args; }
        }
    }
    public class Controller
    {
        HttpClient _client;
        
        public Controller(string uri)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(uri);
        }
        //internal async Task<IEnumerable<T>> Get<T>(params object[] args)
        //{
        //    string callurl;
        //    IEnumerable<T> callResult = new List<T>();
        //    if (args.Length == 0)
        //    {
        //        callurl = $"get{typeof(T).Name.ToLower()}s";
        //        return await GetCall<IEnumerable<T>>(callurl);
        //    }
        //    else
        //    {
        //        List<T> result = new List<T>();
        //        foreach (var arg in args)
        //        {
        //            callurl = $"get{typeof(T).Name.ToLower()}byid/?id={arg}";
        //            result.Add(await GetCall<T>(callurl));
        //        }
        //        return result;
        //    }
        //}

        public async Task<IEnumerable<T>> Get<T>(ControllerGetArgs getArgs = null )
        {
            if (getArgs == null) getArgs = new ControllerGetArgs(); 

            var arguments = getArgs.Args;
            
            string callurl;
            IEnumerable<T> callResult = new List<T>();
            if (arguments == null || arguments.Count == 0)
            {
                callurl = $"get{typeof(T).Name.ToLower()}s";
                return await GetCall<IEnumerable<T>>(callurl);
            }
            else
            {
                List<T> result = new List<T>();
                foreach (var argument in arguments)
                {
                    //string paramName = getUnderlyingName<T>(argument.Key.Name);
                    string paramName = argument.Key;
                    callurl = $"get{typeof(T).Name.ToLower()}by{paramName}/?{paramName}={argument.Value}";
                    result.Add(await GetCall<T>(callurl));
                }
                return result;
            }
        }

        //private string getUnderlyingName<T>(string propertyName)
        //{
        //    string result = String.Empty;
        //    result = propertyName.Substring( typeof(T).Name.Length );
        //    return result.ToLower();
        //}

        public async Task<HttpResponseMessage> Post<T>(T value)
        {
            string callurl = $"add{typeof(T).Name.ToLower()}";

            return await _client.PostAsJsonAsync(callurl, value);
        }

        private async Task<T> GetCall<T>(string callurl)
        {
            HttpResponseMessage response = await _client.GetAsync(callurl);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<T>();
        }


    }
}

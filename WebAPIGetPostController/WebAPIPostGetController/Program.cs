﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model;
using System.Net.Http;
using System.Threading;

namespace WebAPIPostGetController
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.RunAsync().GetAwaiter().GetResult();
            Console.ReadKey();
        }

        private async Task RunAsync()
        {
            Controller c = new Controller("http://localhost:53864/api/values/");

            //await c.Post<Student>(new Student() { StudentId = 102, StudentName = "Karthik" });

            //Getting initial status of db
            ControllerGetArgs getArgs = new ControllerGetArgs();
            //getArgs.Args.Add(//typeof(Student).GetProperty("StudentId")
            //    "id", 102);
            IEnumerable<Student> students = await c.Get<Student>(getArgs);

            Console.WriteLine("Initial number of students: " + students.Count());

            getArgs.Clear();

            //getArgs.Args.Add(//typeof(Student).GetProperty("StudentName")
            //    "name", "Karthik");

            students = await c.Get<Student>(getArgs);


            //IEnumerable<Student> students = await c.Get<Student>();
            Console.WriteLine("Initial number of students: " + students.Count());

            //Posting student object s
            Student s = new Student() { StudentId = 104, StudentName = "Karthik" };
            HttpResponseMessage response = await c.Post<Student>(s);

            if (!response.IsSuccessStatusCode)
            {
                    Console.WriteLine(
                            (response.StatusCode == System.Net.HttpStatusCode.Ambiguous)
                            ?"Same object already exists."
                            :response.StatusCode.ToString()
                        );
            }
            else
            {
                Console.WriteLine("Post Success");
            }

            //Getting final status of db
            students = await c.Get<Student>();
            Console.WriteLine("Final number of students: " + students.Count());

        }
    }
}

drop table if exists tblUserLoan
drop table if exists tblLoan
drop table if exists tblLoanStatus
drop table if exists tblUserRole
drop table if exists tblSetting
drop table if exists tblUserAlert
drop table if exists tblAlert
drop table if exists tblRole
drop table if exists tblUser
create table tblUser (
	tblUserID uniqueidentifier primary key default newid(),
	[Name] varchar(50) not null,
	[Email] varchar(20) not null,
	[Mobile] varchar(10) not null,
	[Password] varchar(20),
	[Address] varchar(50) not null,
	IsActive bit not null
)
create table tblSetting (
	tblSettingID uniqueidentifier primary key default newid(),
	[tblUserID] uniqueidentifier foreign key references [tblUser]([tblUSerID]) not null,
	[IsAllSettingsEnabled] bit not null,
	[IsPushNotificationsEnabled] bit not null,
	[IsAlertsEnabled] bit not null
)
create table tblAlert(
	tblAlertID uniqueidentifier primary key default newid(),
	[Subject] varchar(50) not null,
	[Description] varchar(50) not null,
	[CreatedAt] datetime not null,
	[IsActive] bit not null
)
create table tblUserAlert(
	tblUserID uniqueidentifier foreign key references [tblUser](tblUserID),
	tblAlertID uniqueidentifier foreign key references [tblAlert](tblAlertID),
	[IsRead] bit not null,
	[IsDeleted] bit not null,
	primary key(tblUserID, tblAlertID)
)
create table tblRole(
	tblRoleID uniqueidentifier primary key default newid(),
	[Name] varchar(50) not null
)
create table tbluserRole(
	tblUserID uniqueidentifier foreign key references [tblUser]([tblUserID]),
	tblRoleID uniqueidentifier foreign key references [tblRole]([tblRoleID]),
	primary key([tblUserID], [tblRoleID])
)
create table tblLoanStatus(
	tblLoanStatusID uniqueidentifier primary key default newid(),
	[Description] varchar(50) not null,
	[Label] varchar(10) not null,
	[IsActive] bit not null
)
create table tblLoan(
	tblLoanID uniqueidentifier primary key default newid(),
	[ActualLoanID] varchar(50) not null,
	[PrimaryBorrowerID] uniqueidentifier foreign key references [tblUser]([tblUserID]),
	[CreatedAt] datetime not null,
	[ModifiedAt] datetime not null,
	[tblLoanStatusID] uniqueidentifier foreign key references [tblLoanStatus]([tblLoanStatusID]),
	[IsActive] bit not null
)
create table tblUserLoan(
	tblLoanID uniqueidentifier foreign key references [tblLoan]([tblLoanID]),
	tblUserID uniqueidentifier foreign key references [tblUser]([tblUserID]),
	tblRoleID uniqueidentifier foreign key references [tblRole]([tblRoleID]),
	primary key(tblLoanID, tblUserID, tblRoleID),
	IsActive bit not null
)

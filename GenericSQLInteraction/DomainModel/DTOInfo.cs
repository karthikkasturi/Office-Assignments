﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DomainModel
{
    public class DTOInfo
    {
        public IEnumerable<Type> GetDTOTypes()
        {
            string nspace = "DomainModel.DTO";
            List<Type> tr = Assembly.GetExecutingAssembly().GetTypes().ToList();
            return tr.Where(t => string.Equals(t.Namespace, nspace)).ToList();
        }
    }
}

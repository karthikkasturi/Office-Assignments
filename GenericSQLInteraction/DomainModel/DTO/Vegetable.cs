﻿namespace DomainModel.DTO
{
    public class Vegetable
    {
        public string Name { get; set; }
        public double PricePerKg { get; set; }
    }
}

﻿namespace DomainModel.DTO
{
    public class Employee
    {
        public int Empno { get; set; }
        public int Mgr { get; set; }
        public string EmpName { get; set; }
    }
}

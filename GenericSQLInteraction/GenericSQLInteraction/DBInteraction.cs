﻿using System.Data;
using System;
using System.Data.SqlClient;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace GenericSQLInteraction
{
    class DBInteraction
    {
        string _connectionString;
        public DBInteraction(string datasource = "ggku4mpc62", string database = "k4rth1k")
        {
            _connectionString = $"Server={datasource};Database={database};Trusted_Connection=SSPI";
        }
        public DataSet Select<T>(T record)
        {
            return SelectTop(record, -1);
        }

        public DataSet SelectTop<T>(T record, int recordsLimit)
        {
            DataSet ds = new DataSet();
            string selectStatement = "select ";
            if (recordsLimit >= 0)
            {
                selectStatement += " top " + recordsLimit + " ";
            }
            using (var conn = new SqlConnection(_connectionString))
            {
                using (var adapter = new SqlDataAdapter($"{selectStatement} * from {record.GetType().Name}", conn))
                {
                    adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                    adapter.Fill(ds);
                }
            }
            return ds;
        }
        public DataSet Insert<T>(T record)
        {
            DataSet ds;
            int v = 0;
            string keys = string.Empty, values = string.Empty;
            foreach (var property in record.GetType().GetProperties())
            {
                if (property.GetType().IsValueType)
                {
                    if (property.GetValue(record).Equals(Activator.CreateInstance(property.GetType())))
                    {
                        continue;
                    }
                    else
                    {
                        keys += ", " + property.Name;
                        values += ", @" + property.Name;
                    }
                }
                else if (property.GetValue(record) != null)
                {
                    keys += ", " + property.Name.ToString();
                    values += ", @" + property.Name.ToString();
                }
            }
            if (keys.Length > 0)
            {
                keys = keys.Substring(1);
                values = values.Substring(1);
            }
            string queryString = $"insert into {record.GetType().Name} ({keys}) values ({values})";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(queryString, conn))
                {
                    foreach (var key in keys.Split(','))
                    {
                        command.Parameters.AddWithValue(
                            key.Trim(), record.GetType().GetProperty(key.Trim())
                                                .GetValue(record));
                    }
                    v = command.ExecuteNonQuery();
                }
                conn.Close();
            }
            ds = CreateRowsAffectedDataSet(v);
            return ds;
        }
        
        public DataSet Update<T>(T record)
        {

            DataSet ds;
            int v = 0;
            string keys = string.Empty, set = string.Empty;
            ds = SelectTop(record, 0);
            DataColumn[] primaryKeys = ds.Tables[0].PrimaryKey;
            foreach (var primaryKey in primaryKeys)
            {
                keys += "and " + primaryKey.ColumnName + " = " + " @" + primaryKey.ColumnName;
            }
            if (keys.Length > 0)
            {
                keys = keys.Substring(3);
            }
            else
            {
                throw new NotSupportedException("Deletion is not supported on tables without primary keys.");
            }
            List<string> primarycols = primaryKeys.Select(m => m.ColumnName).ToList();
            IEnumerable<PropertyInfo> nonPrimaryProperties = record.GetType().GetProperties()
                .Where(m => !primarycols.Contains(m.Name, StringComparer.InvariantCultureIgnoreCase));
            foreach (PropertyInfo property in nonPrimaryProperties)
            {
                if (property.GetType().IsValueType)
                {
                    if (property.GetValue(record).Equals(Activator.CreateInstance(property.GetType())))
                    {
                        continue;
                    }
                    else
                    {
                        set += ", " + property.Name + " = " + " @" + property.Name + " ";
                    }
                }
                else if (property.GetValue(record) != null)
                {
                    set += ", " + property.Name + " = " + " @" + property.Name + " ";
                }
            }
            if (set.Length > 0)
            {
                set = set.Substring(1);
            }
            else
            {
                throw new NotSupportedException("No data change sent for update.");
            }
            string queryString = $"update {record.GetType().Name} set {set} where {keys}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(queryString, conn))
                {
                    foreach (var primaryKey in primaryKeys)
                    {
                        var property = record.GetType().GetProperty(primaryKey.ColumnName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        command.Parameters.AddWithValue(primaryKey.ColumnName, property.GetValue(record));
                    }
                    foreach (string seti in set.Split('@').Skip(1))
                    {
                        string propName = seti.Split(' ')[0];
                        var property = record.GetType().GetProperty(propName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        command.Parameters.AddWithValue(propName, property.GetValue(record));
                    }
                    v = command.ExecuteNonQuery();
                }
                conn.Close();
            }
            ds = CreateRowsAffectedDataSet(v);
            return ds;
            
        }
        public DataSet Delete<T>(T record)
        {
            DataSet ds;
            int v = 0;
            string keys = string.Empty;
            ds = SelectTop(record, 0);
            DataColumn[] primaryKeys = ds.Tables[0].PrimaryKey;
            foreach (var primaryKey in primaryKeys)
            {
                keys += "and " + primaryKey.ColumnName + " = " + " @" + primaryKey.ColumnName;
            }
            if (keys.Length > 0)
            {
                keys = keys.Substring(3);
            }
            else
            {
                throw new NotSupportedException("Deletion is not supported on tables without primary keys.");
            }
            string queryString = $"delete from {record.GetType().Name} where {keys}";
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(queryString, conn))
                {
                    foreach (var primaryKey in primaryKeys)
                    {
                        var property = record.GetType().GetProperty(primaryKey.ColumnName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        command.Parameters.AddWithValue(primaryKey.ColumnName, property.GetValue(record));
                    }
                    v = command.ExecuteNonQuery();
                }
                conn.Close();
            }
            ds = CreateRowsAffectedDataSet(v);
            return ds;
        }
        
        private DataSet CreateRowsAffectedDataSet(int v)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Rows Afftected", typeof(int)));
            DataRow dr = dt.NewRow();
            dr[0] = v;
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);
            return ds;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DomainModel;
using System.Data;

namespace GenericSQLInteraction
{

    delegate void ListOptions();
    delegate object RelevantInputReads(object record);
    class Program
    {
        List<Type> _dtoList;
        List<string> _sqlCommands = new List<string>() { "Select", "Insert", "Update", "Delete" };
        List<RelevantInputReads> inputReads = new List<RelevantInputReads>();
        Program()
        {
            inputReads.Add(new RelevantInputReads(ReadNothing));
            inputReads.Add(new RelevantInputReads(ReadPrimaryInputs) + ReadNonPrimaryInputs);
            inputReads.Add(new RelevantInputReads(ReadPrimaryInputs) + ReadNonPrimaryInputs);
            inputReads.Add(new RelevantInputReads(ReadPrimaryInputs));
        }
        static void Main(string[] args)
        {
            DTOInfo dti = new DTOInfo();
            Program p = new Program();
            p._dtoList = dti.GetDTOTypes().ToList();
            DBInteraction dbInteraction = new DBInteraction("ggku4mpc62", "k4rth1k");
            //while (true)
            //{
            int selectedTableIndex = p.ReadChoice(p.ListDTOs, p._dtoList.Count());
            if (selectedTableIndex == 0)
                return;
            selectedTableIndex--;
            int selectedCommand = p.ReadChoice(p.ListSQLCommands, p._sqlCommands.Count());
            if (selectedCommand == 0)
                return;
            selectedCommand--;
            object record = p.inputReads[selectedCommand].Invoke(Activator.CreateInstance(p._dtoList[selectedTableIndex]));
            MethodInfo method = typeof(DBInteraction).GetMethod(p._sqlCommands[selectedCommand]);
            method = method.MakeGenericMethod(p._dtoList[selectedTableIndex]);
            DataSet result = (DataSet)method.Invoke(dbInteraction, new[] { record });
            p.PrintDataSet(result);
            Console.ReadLine();
            //}
        }

        private void PrintDataSet(DataSet dataSet)
        {
            DataTableReader dr = dataSet.CreateDataReader();
            foreach (DataColumn x in dataSet.Tables[0].Columns)
            {
                Console.Write(x.ColumnName + "\t");
            }
            Console.WriteLine();
            while (dr.Read())
            {
                for (int i = 0; i < dr.FieldCount; i++)
                    Console.Write(dr[i] + "\t");
                Console.WriteLine();
            }
            dr.Close();
        }

        private int ReadChoice(ListOptions listOptions, int maxCount)
        {
            int choice;
            listOptions();
            Console.Write("Choose the option#: ");
            if (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Incorrect input. Select option number from indices.");
                return ReadChoice(listOptions, maxCount);
            }
            if (choice > maxCount)
            {
                Console.WriteLine("Incorrect Selection. Select proper index.");
                return ReadChoice(listOptions, maxCount);
            }
            return choice;
        }

        private void ListDTOs()
        {
            Console.WriteLine("Available Tables: ");
            int count = 1;
            foreach (Type t in _dtoList)
            {
                Console.WriteLine(count + ") " + t.Name);
                count++;
            }

        }
        private void ListSQLCommands()
        {
            Console.WriteLine("Available Commands: ");
            int count = 1;
            foreach (string t in _sqlCommands)
            {
                Console.WriteLine(count + ") " + t);
                count++;
            }
        }

        T ReadNonPrimaryInputs<T>(T record)
        {
            DataSet ds = new DBInteraction().SelectTop(record, 0);
            DataColumn[] primaryKeys = ds.Tables[0].PrimaryKey;
            Console.WriteLine("Please input relevant keys: ");
            foreach (PropertyInfo propertyInfo in record.GetType().GetProperties())
            {
                if (primaryKeys
                    .Where(m => m.ColumnName.Equals(propertyInfo.Name, StringComparison.InvariantCultureIgnoreCase))
                    .ToList()
                    .Count > 0)
                {
                    continue;
                }

                Console.Write($"Enter {propertyInfo.Name}({propertyInfo.PropertyType.Name}): ");
                try
                {
                    object value = Convert.ChangeType(Console.ReadLine(), propertyInfo.PropertyType);
                    propertyInfo.SetValue(record, value);
                }
                catch
                {

                }
            }

            return record;
        }

        T ReadNothing<T>(T record)
        {
            return record;
        }

        T ReadPrimaryInputs<T>(T record)
        {
            DataSet ds = new DBInteraction().SelectTop(record, 0);
            DataColumn[] primaryKeys = ds.Tables[0].PrimaryKey;
            if (primaryKeys.Length == 0)
            {
                throw new NotSupportedException("Updation and Deleted is not supported on tables without primary keys.");
            }
            Console.WriteLine("Please fill the primary keys: ");
            foreach (DataColumn primaryKey in primaryKeys)
            {
                PropertyInfo propertyInfo = record.GetType()
                    .GetProperty(primaryKey.ColumnName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                Console.Write($"Enter {propertyInfo.Name}({propertyInfo.PropertyType.Name}): ");
                try
                {
                    string input = Console.ReadLine();
                    if (!string.IsNullOrEmpty(input) && !string.IsNullOrWhiteSpace(input))
                    {
                        Console.WriteLine(propertyInfo.PropertyType);
                        object value = Convert.ChangeType(input, propertyInfo.PropertyType);
                        propertyInfo.SetValue(record, value);
                    }
                }
                catch
                {
                    Console.WriteLine("Improper input. May cause error.");
                }
            }

            return record;
        }



    }
}
